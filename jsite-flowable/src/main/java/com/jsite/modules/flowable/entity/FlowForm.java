/**
 * Copyright &copy; 2017-2019 <a href="https://gitee.com/baseweb/JSite">JSite</a> All rights reserved.
 */
package com.jsite.modules.flowable.entity;

import com.google.common.collect.Lists;
import com.jsite.common.lang.StringUtils;
import com.jsite.common.persistence.DataEntity;
import com.jsite.modules.sys.entity.User;
import org.hibernate.validator.constraints.Length;

import java.util.List;

/**
 * 流程表单生成Entity
 * @author liuruijun
 * @version 2019-03-21
 */
public class FlowForm extends DataEntity<FlowForm> {
	
	private static final long serialVersionUID = 1L;
	private String modelId;			// 流程模型id
	private String modelKey;		// 流程模型key
	private String modelVersion;		// 流程模型版本
	private String formContent;		// 表单内容

	private String modelName;

    private List<User> userList = Lists.newArrayList(); // 拥有用户列表

	public FlowForm() {
		super();
	}

	public FlowForm(String modelKey, String  modelVersion) {
        super();
        this.modelKey = modelKey;
        this.modelVersion = modelVersion;
	}

	public FlowForm(String id){
		super(id);
	}

    @Length(min=0, max=255, message="流程模型key长度必须介于 0 和 255 之间")
	public String getModelId() {
		return modelId;
	}

	public void setModelId(String modelId) {
		this.modelId = modelId;
	}

	@Length(min=0, max=400, message="流程模型key长度必须介于 0 和 400 之间")
	public String getModelKey() {
		return modelKey;
	}

	public void setModelKey(String modelKey) {
		this.modelKey = modelKey;
	}
	
	@Length(min=0, max=11, message="流程模型版本长度必须介于 0 和 11 之间")
	public String getModelVersion() {
		return modelVersion;
	}

	public void setModelVersion(String modelVersion) {
		this.modelVersion = modelVersion;
	}
	
	public String getFormContent() {
		return formContent;
	}

	public void setFormContent(String formContent) {
		this.formContent = formContent;
	}

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public List<User> getUserList() {
		return userList;
	}

	public void setUserList(List<User> userList) {
		this.userList = userList;
	}

	public List<String> getUserIdList() {
		List<String> userIdList = Lists.newArrayList();
		for (User user : userList) {
            userIdList.add(user.getId());
		}
		return userIdList;
	}

	public void setUserIdList(List<String> userIdList) {
        userList = Lists.newArrayList();
		for (String userId : userIdList) {
			User user = new User();
			user.setId(userId);
            userList.add(user);
		}
	}

	public String getUserIds() {
		return StringUtils.join(getUserIdList(), ",");
	}
	//SpringMVC自动装配
	public void setUserIds(String userIds) {
        userList = Lists.newArrayList();
		if (userIds != null){
			String[] ids = StringUtils.split(userIds, ",");
			setUserIdList(Lists.newArrayList(ids));
		}
	}
}