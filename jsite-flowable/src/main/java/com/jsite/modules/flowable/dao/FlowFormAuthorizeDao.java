/**
 * Copyright &copy; 2017-2019 <a href="https://gitee.com/baseweb/JSite">JSite</a> All rights reserved.
 */
package com.jsite.modules.flowable.dao;

import com.jsite.common.persistence.CrudDao;
import com.jsite.common.persistence.annotation.MyBatisDao;
import com.jsite.modules.flowable.entity.TaskNode;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 流程表单生成DAO接口
 * @author liuruijun
 * @version 2019-03-28
 */
@MyBatisDao
public interface FlowFormAuthorizeDao extends CrudDao<TaskNode> {
	List<String> findFieldByKeyVer(@Param("modelKey") String modelKey, @Param("modelVersion") String modelVersion);
	List<TaskNode> findTaskNodesByKeyVer(@Param("modelKey") String modelKey, @Param("modelVersion") String modelVersion);

	// 暂未实现，权限信息加载时，根据最新模型json，过滤掉修改过的或者删除的节点，保存时删除原有权限信息，保存最新数据
	void deleteByFieldAndKeyVersion(TaskNode taskNode);
}