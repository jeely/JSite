/**
 * Copyright &copy; 2017-2019 <a href="https://gitee.com/baseweb/JSite">JSite</a> All rights reserved.
 */
package com.jsite.modules.flowable.dao;

import com.jsite.common.persistence.CrudDao;
import com.jsite.common.persistence.annotation.MyBatisDao;
import com.jsite.modules.flowable.entity.TaskNode;

/**
 * 流程表单生成DAO接口
 * @author liuruijun
 * @version 2019-04-28
 */
@MyBatisDao
public interface FlowFormVarsDao extends CrudDao<TaskNode> {
	TaskNode findFormVarByField(TaskNode node);
}